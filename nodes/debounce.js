module.exports = function(RED) {
    function Debounce(config) {
        RED.nodes.createNode(this,config);
        var node = this;

        node.time = Math.max(config.time || 1000, 0);
        node.name = config.name;

        var context = this.context();
        node.on('input', function(msg) {
            var cansend = context.get('debounce-cansend');
            if (cansend !== false) {
                context.set('debounce-cansend', false);
                node.status({ fill: "red", shape: "dot", text: "deactivated" });
                var timeout = setTimeout(function() {
                    context.set('debounce-cansend', true);
                    node.status({ fill: "green", shape: "dot", text: "ready" })
                }, msg.time || node.time);
                node.send([ msg, null]);
            } else {
                node.send([ null, msg ]);
            }
        });
    }
    RED.nodes.registerType("debounce", Debounce);
}
